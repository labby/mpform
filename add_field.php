<?php

/**
 *
 * 
 *  @module         MPForm
 *  @author         Frank Heyne, Dietrich Roland Pehlke, erpe
 *  @license        see info.php of this addon
 *  @platform       see info.php of this addon
 *  @license terms  see info.php of this addon
 *  @version        see info.php of this module
 *  
 *
 */

if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}

// Include admin wrapper script
require(LEPTON_PATH.'/modules/admin.php');

// Get new order
$order = LEPTON_order::getInstance(TABLE_PREFIX.'mod_mpform_fields', 'position', 'field_id', 'section_id');
$position = $order->get_new($section_id);

// Insert new row into database
$fields = array(
	'section_id'	=> $section_id,
	'page_id'		=> $page_id,
	'position'		=> $position,
	'required'		=> '0',
	'value'			=> '',
	'extra'			=> ''
);
$database->build_and_execute(
	'insert',
	TABLE_PREFIX."mod_mpform_fields",
	$fields
);

// Get the id
$field_id = $database->get_one("SELECT LAST_INSERT_ID()");

// Check whether results table exists, create it if not
$list = $database->list_tables(TABLE_PREFIX);
if(!in_array("mod_mpform_results_".$section_id,$list)) {
	//	create table
	$table_fields="
		`session_id` VARCHAR(20) NOT NULL,
		`started_when` INT NOT NULL DEFAULT '0',
		`submitted_when` INT NOT NULL DEFAULT '0',
		`referer` varchar(255) NOT NULL DEFAULT '',
		 PRIMARY KEY ( `session_id` )
		";
	LEPTON_handle::install_table("mod_mpform_results_".$section_id, $table_fields);	
}

// Insert new column into database
$database->simple_query("ALTER TABLE ".TABLE_PREFIX ."mod_mpform_results_".$section_id." ADD field".$field_id." TEXT NOT NULL");

// Say that a new record has been added, then redirect to modify page
if($database->is_error()) {
	$admin->print_error($database->get_error(), LEPTON_URL.'/modules/mpform/modify_field.php?page_id='.$page_id.'&section_id='.$section_id.'&field_id='.$field_id);
	} else {
	$admin->print_success($TEXT['SUCCESS'], LEPTON_URL.'/modules/mpform/modify_field.php?page_id='.$page_id.'&section_id='.$section_id.'&field_id='.$field_id);
}

// Print admin footer
$admin->print_footer();

?>