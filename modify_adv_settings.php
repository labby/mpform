<?php

/**
 *
 * 
 *  @module         MPForm
 *  @author         Frank Heyne, Dietrich Roland Pehlke, erpe
 *  @license        see info.php of this addon
 *  @platform       see info.php of this addon
 *  @license terms  see info.php of this addon
 *  @version        see info.php of this module
 *  
 *
 */

if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}

// get class and admin
$oMPFORM = mpform::getInstance();
$mod_dir = $oMPFORM->module_directory;



// obtain display option from the database table
$settings = array();
$database->execute_query(
	"SELECT * FROM `".TABLE_PREFIX . "mod_mpform_settings` WHERE `section_id` = ".$section_id,
	true,
	$settings,
	false
);

// protect from cross page reading
if ($settings['page_id'] != $page_id) {  
	header("Location: ".ADMIN_URL."/pages/index.php");
	exit(0);
}

// replace static template placeholders with values from language file
$form_values =	array(
	// variables from framework
	'oMPF'				=> $oMPFORM,
	'settings'			=> $settings,
	'PAGE_ID'			=> (int) $page_id,
	'SECTION_ID'		=> (int) $section_id,
	'MODULE_URL'		=> LEPTON_URL . "/modules/mpform",
	'MEDIA_DIRECTORY'   => LEPTON_PATH . MEDIA_DIRECTORY,
		// module settings
	'MOD_SAVE_URL'				=> LEPTON_URL. str_replace("\\","/",substr(dirname(__FILE__),strlen(LEPTON_PATH))).'/save_adv_settings.php'
);


$oTWIG = lib_twig_box::getInstance();
$oTWIG->registerModule( $mod_dir );

echo $oTWIG->render(
	"@mpform/backend_modify_adv_settings.lte",
	$form_values
);

$oMPFORM->admin->print_footer();
?>