<?php

/**
 *  @module         MPForm
 *  @author         Frank Heyne, Dietrich Roland Pehlke, erpe
 *  @license        see info.php of this addon
 *  @platform       see info.php of this addon
 *  @license terms  see info.php of this addon
 *  @version        see info.php of this module
 *
 */

if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}

// get class and admin
$oMPFORM = mpform::getInstance();
$mod_dir = $oMPFORM->module_directory;

$oTWIG = lib_twig_box::getInstance();
$oTWIG->registerModule( $mod_dir );

/** 
 *	Make sure that page and section id are numeric.
 */
$page_id = (isset($page_id)) ? intval($page_id) : 0;
$section_id = (isset($section_id)) ? intval($section_id ): 0;

// Query submissions table
$order_submissions=array();
$all_submissions = array();
$database->execute_query(
	"SELECT * FROM `".TABLE_PREFIX."mod_mpform_submissions` WHERE `section_id` = '".$section_id."' ORDER BY `submitted_when` DESC",
	true,
	$all_submissions
);

if(count($all_submissions) > 0) {
	// List submissions
	foreach($all_submissions as &$submission) {	
		$order_submissions[]= array(
				'SUBMISSION_ID'	=> $submission['submission_id'],
				'field_submission_id'	=> $submission['submission_id'],
				'field_submission_when'	=> date(TIME_FORMAT.', '.DATE_FORMAT, $submission['submitted_when'])
		);
	}
}

$form_values = array(
	// variables from framework
	'oMPF'			=> $oMPFORM,
	'PAGE_ID'		=> (int) $page_id,
	'SECTION_ID'	=> (int) $section_id,
	'MODULE_URL'    => LEPTON_URL.'/modules/'.$mod_dir,
	"order_submissions"	=> $order_submissions,
	"leptoken"		=> get_leptoken()
);

echo $oTWIG->render(
	'@mpform/backend_delete_submissions.lte',
	$form_values
);
// Print admin footer
$oMPFORM->admin->print_footer();

?>