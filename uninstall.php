<?php

/**
 *
 * 
 *  @module         MPForm
 *  @author         Frank Heyne, Dietrich Roland Pehlke, erpe
 *  @license        see info.php of this addon
 *  @platform       see info.php of this addon
 *  @license terms  see info.php of this addon
 *  @version        see info.php of this module
 *  
 *
 */
 
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}

// delete search table entries
$database->execute_query("DELETE FROM ".TABLE_PREFIX."search WHERE name = 'module' AND value = 'mpform'");
$database->execute_query("DELETE FROM ".TABLE_PREFIX."search WHERE extra = 'mpform'");

// drop tables
LEPTON_handle::drop_table("mod_mpform_fields");
LEPTON_handle::drop_table("mod_mpform_settings");
LEPTON_handle::drop_table("mod_mpform_submissions");

//drop result tables
$table_name = TABLE_PREFIX . "mod_mpform_results_%";
$result_tables = [];
$database->execute_query(
	"SHOW TABLES LIKE '".$table_name."'",
	true,
	$result_tables,
	true
);	
if (count($result_tables) > 0 )
{
	foreach ($result_tables as $to_delete)
    {
		LEPTON_handle::drop_table($to_delete);
	}
}

LEPTON_handle::removeAllThemeFiles(__FILE__);
