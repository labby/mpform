<?php

/**
 *
 * 
 *  @module         MPForm
 *  @author         Frank Heyne, Dietrich Roland Pehlke, erpe
 *  @license        see info.php of this addon
 *  @platform       see info.php of this addon
 *  @license terms  see info.php of this addon
 *  @version        see info.php of this module
 *  
 *
 */

if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}

$admin = LEPTON_admin::getInstance('Pages', 'pages_modify');

?>
<div class="ui grey segment">
	<h2 class="ui header">MP Form Help</h2>
	<div class="helppage">
		<p>The help and documentation for this module now consists of approximately 20 pages. It is available online at the <a href="https://doc.lepton-cms.org/documentation/mp_form/EN/mpform.html" target="_blank">LEPTON documentation</a>.</p>
		<p>If you find bugs, please <a href="http://forum.lepton-cms.org/" target="_blank">report them</a>.</p>
		<p>New since version 2.3.3: code modified for use of individual css styles for radio buttons and checkboxes as proposed by <a href="https://www.w3schools.com/howto/howto_css_custom_checkbox.asp" target="_blank">w3schools.com</a>.</p>
	</div>
	<br />
	<table cellpadding="0" cellspacing="0" border="0" width="100%">
		<tr>
			<td align="center">
				<input class="helppage" type="button" value="<?php echo $TEXT['BACK']; ?>" onclick="javascript: window.location = '<?php echo ADMIN_URL; ?>/pages/modify.php?page_id=<?php echo $page_id; ?>';" style="width: 100px; margin-top: 5px;" />
			</td>
		</tr>
	</table>
</div>	
<?php
$admin->print_footer();
?>