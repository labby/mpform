<?php

/**
 *
 * 
 *  @module         MPForm
 *  @author         Frank Heyne, Dietrich Roland Pehlke, erpe
 *  @license        see info.php of this addon
 *  @platform       see info.php of this addon
 *  @license terms  see info.php of this addon
 *  @version        see info.php of this module
 *  
 *
 */

if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}

// get class and admin
global $page_id, $section_id;
$oMPFORM = mpform::getInstance();

// set default values
$header = '<table cellpadding="2" cellspacing="0" border="0">';
$field_loop = '<tr class="{CLASSES}"><td class="mpform_title">{TITLE}{REQUIRED}:</td><td class="mpform_help">{HELP}</td><td class="mpform_field">{FIELD}{ERRORTEXT}</td></tr>';
$footer = '<tr><td></td><td></td>
<td><input type="submit" name="submit" class="mpform_submit" value="'.$oMPFORM->language['backend']['TXT_SUBMIT'].'" /></td></tr>
</table>';
$heading_html = "<h3>{HEADING}</h3>";
$short_html = "<b>{TITLE}:</b> {DATA}<br />";
$long_html = "<b>{TITLE}:</b><br />{DATA}<br /><br />";
$email_html = "<b>{TITLE}:</b> <a href=\"mailto:{DATA}\">{DATA}</a><br />";
$uploadfile_html = "<b>{TITLE}:</b> <a href=\"{DATA}\">{DATA}</a><br />";
$date_format = $oMPFORM->language['backend_adv']['date_format'];
$is_following = 0;
$upload_files_folder = MEDIA_DIRECTORY. "/".$oMPFORM->module_directory;
$email_to = $oMPFORM->admin->getValue("email", "string", "session");
$email_from = '';
$email_fromname = '';
$email_subject = $oMPFORM->language['backend']['EMAIL_SUBJECT'];
$success_page = 'none';
$success_text = '<div class="mpform_results">Thank you for submitting your data. We received the following data:<br />{DATA}
<br />Referer page: {REFERER}<br />Your IP address: {IP}</div>';
$submissions_text = '{DATA}
Referer page: {REFERER}
IP address: {IP}
Date: {DATE}';
$email_text = 'The following data was submitted:<br />{DATA}
<br />Referer page: {REFERER}<br />IP address: {IP}';
$success_email_to = '';
$success_email_from = $oMPFORM->admin->getValue("email", "string", "session");
$success_email_fromname = '';
$success_email_text = 'Thank you for submitting your data. We received the following data:<br />{DATA}
<br />Referer page: {REFERER}<br />Your IP address: {IP}';
$success_email_subject = $oMPFORM->language['backend']['EMAIL_SUC_SUBJ'];
$max_submissions = 50;
$stored_submissions = 1000;
$max_file_size_kb = 1024;
$attach_file = 0;
$upload_file_mask = '0604';
$upload_dir_mask = '0705';
$upload_only_exts = "jpg,gif,png,tif,bmp,pdf";
if(extension_loaded('gd') AND function_exists('imageCreateFromJpeg')) { /* Make's sure GD library is installed */
	$use_captcha = true;
} else {
	$use_captcha = false;
}

// Insert row into database
$fields = array(
	'page_id'		=> $page_id,
	'section_id'	=> $section_id,
	'header'		=> $header,
	'field_loop'	=> $field_loop,
	'footer'		=> $footer,
	'email_to'		=> $email_to,
	'email_from'	=> $email_from,
	'email_fromname'=> $email_fromname,
	'email_subject'	=> $email_subject,
	'email_text'	=> $email_text,
	'success_page'	=> $success_page,
	'success_text'	=> $success_text,	
	'submissions_text'	=> $submissions_text,
	'success_email_to'	=> $success_email_to,
	'success_email_from'=> $success_email_from,
	'success_email_fromname'	=> $success_email_fromname,
	'success_email_text'	=> $success_email_text,
	'success_email_subject'	=> $success_email_subject,
	'stored_submissions'	=> $stored_submissions,	
	'max_submissions'	=> $max_submissions,
	'heading_html'	=> $heading_html,
	'short_html'	=> $short_html,
	'long_html'		=> $long_html,
	'email_html'	=> $email_html,
	'uploadfile_html'	=> $uploadfile_html,
	'use_captcha'	=> $use_captcha,
	'upload_files_folder'	=> $upload_files_folder,
	'date_format'	=> $date_format,
	'max_file_size_kb'	=> $max_file_size_kb,
	'attach_file'	=> $attach_file,
	'upload_file_mask'	=> $upload_file_mask,
	'upload_dir_mask'	=> $upload_dir_mask,
	'upload_only_exts'	=> $upload_only_exts,
	'is_following'	=> $is_following,
	'tbl_suffix'	=> $section_id
);
LEPTON_database::getInstance()->build_and_execute(
	'insert',
	TABLE_PREFIX."mod_mpform_settings",
	$fields
);

