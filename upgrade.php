<?php

/**
 *
 * 
 *  @module         MPForm
 *  @author         Frank Heyne, Dietrich Roland Pehlke, erpe
 *  @license        see info.php of this addon
 *  @platform       see info.php of this addon
 *  @license terms  see info.php of this addon
 *  @version        see info.php of this module
 *  
 *
 */

// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}
// end include secure.php

//from LEPTON 4.0
LEPTON_handle::delete_obsolete_directories(['/modules/mpform/htt']);

$files = array (
	'/modules/mpform/register_parser.php',
	'/modules/mpform/register_language.php',
	'/modules/mpform/functions.php',
	'/modules/mpform/templates/backend_header.lte'
);
LEPTON_handle::delete_obsolete_files($files);

if (file_exists (LEPTON_PATH.'/modules/mpform/private.php')) 
{
	rename( LEPTON_PATH.'/modules/mpform/private.php',LEPTON_PATH .'/modules/mpform/private_sik.php');
}

// update to release 2.3.4
$files = array (
	'/modules/mpform/js/dragdrop.js',
	'/modules/mpform/footers.inc.php'
);
LEPTON_handle::delete_obsolete_files($files);

LEPTON_handle::moveThemeFiles( __FILE__ );

