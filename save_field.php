<?php

/**
 *
 * 
 *  @module         MPForm
 *  @author         Frank Heyne, Dietrich Roland Pehlke, erpe
 *  @license        see info.php of this addon
 *  @platform       see info.php of this addon
 *  @license terms  see info.php of this addon
 *  @version        see info.php of this module
 *  
 *
 */

if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}

// Get id
if(!isset($_POST['field_id']) OR !is_numeric($_POST['field_id'])) {
	header("Location: ".ADMIN_URL."/pages/index.php");
	exit(0);
} else {
	$field_id = (int) $_POST['field_id'];
}

require_once LEPTON_PATH.'/modules/mpform/constants.php';

// Include admin wrapper script
$update_when_modified = true; // Tells script to update when this page was last updated
require_once LEPTON_PATH.'/modules/admin.php';

$fid = $field_id;

// obtain module directory
$mod_dir = basename(dirname(__FILE__));

function int_not0($s) {
	$i = intval($s);
	return (($i==0)?'':$i);
}

$post_title = $admin->getValue("title", "string") ?? "";
$post_type  = $admin->getValue("type", "string") ?? "";

// no need for the user to put a title in the end of a fieldset or html code:
if($post_title == '' AND $post_type == 'fieldset_end') $_POST['title'] = "end of fieldset";
if($post_title == '' AND $post_type == 'html') 		   $_POST['title'] = "HTML code";

// Validate all fields
if ($post_title == '' OR $post_type == '') 
{
	$admin->print_error($MESSAGE['GENERIC_FILL_IN_ALL'], LEPTON_URL.'/modules/mpform/modify_field.php?page_id='.$page_id.'&section_id='.$section_id.'&field_id='.$fid);
} 
else 
{
	$title		= str_replace(array("[[", "]]"), '', htmlspecialchars($post_title, ENT_QUOTES));
	$type 		= str_replace(array("[[", "]]"), '', $post_type);
	if (isset($_POST['required'])) 
	{
		$required = $admin->getValue('required');
	} 
	else 
	{
		$required = '0';
	}
	$help   = str_replace(array("[[", "]]"), '', htmlspecialchars($admin->getValue('help', 'string') ?? "", ENT_QUOTES));
}

// is this a new field or an attack?
$broken = true;
$field_info = [];
$database->execute_query(
	"SELECT * FROM ".TABLE_PREFIX."mod_mpform_fields WHERE field_id = ".$field_id." ",
	true,
	$field_info,
	false
);

if(!empty($field_info)) 
{
	$isnewfield = $field_info['title'] == "";
	$broken = $field_info['page_id'] != $page_id;
}

if ($broken) 
{
	header("Location: ".ADMIN_URL."/pages/index.php");
	exit(0);	
}

// Update row
$database->simple_query("UPDATE ".TABLE_PREFIX."mod_mpform_fields SET title = '".$title."', type = '".$type."', required = ".$required.", help = '".$help."' WHERE field_id = ".$field_id." ");

// If field type has multiple options, get all values and implode them
$value = '';
$list_count = addslashes($admin->getValue('list_count') ?? "");
if(is_numeric($list_count)) 
{
	$values = [];
	for($i = 1; $i <= $list_count; $i++) 
	{
		if (isset($_POST['isdefault']) and (is_numeric($_POST['isdefault']))) 
		{
			$default = $_POST['isdefault'];
		} 
		elseif (isset($_POST['isdefault'.$i]) and (is_numeric($_POST['isdefault'.$i]))) 
		{
			$default = $_POST['isdefault'.$i];
		} 
		else 
		{
			$default = 0;
		}
		if($admin->getValue('value'.$i) != '') 
		{
			($default == $i) ? $defcode = IS_DEFAULT : $defcode = '';
			$values[] = str_replace(
			    array("[[", "]]"),
			    '',
			    str_replace(
			        ",",
			        "&#44;",
			        htmlspecialchars(
			            $admin->getValue('value'.$i)
			        )
			    )
			) . $defcode;
		}
	}
	$value = implode(',', $values);
}

// Get extra fields for field-type-specific settings
// Validate all fields and translate special chars
$fields = [];
$field_type = $post_type;
switch( $field_type ) {
	
	case 'textfield':
 	case 'email_subj':
	case 'email':
	case 'integer_number':
	case 'decimal_number':
	case 'filename':
		$fields = array(
			'value' => str_replace(array("[[", "]]"), '', htmlspecialchars($admin->getValue('value') ?? "", ENT_QUOTES)),
			'extra' => int_not0($admin->getValue('length'))
		);
		break;
	
	case 'textarea':
		$width = int_not0($admin->getValue('width'));
		$rows  = int_not0($admin->getValue('rows'));
		
		$fields = array(
			'value'	=> str_replace(array("[[", "]]"), '', htmlspecialchars($admin->getValue('value') ?? "", ENT_QUOTES)),
			'extra'	=> $width.",".$rows
		);
		break;
		
	case 'html':
		$fields = array(
			'value'	=> str_replace(array("[[", "]]"), '', htmlspecialchars($admin->getValue('value') ?? "", ENT_QUOTES))
		);
		break;
		
	case 'heading':
		$extra = str_replace(array("[[", "]]"), '', htmlspecialchars($admin->getValue('template') ?? "", ENT_QUOTES));
		if(trim($extra) == '') $extra = '<tr><td class="mpform_heading" colspan="3">{TITLE}{FIELD}</td></tr>';
		
		$fields = array(
			'value'	=> "",
			'extra'	=> $extra
		);
		break;
		
	case 'select':
		$fields = array(
			'value'	=> $value, 
			'extra'	=> int_not0($admin->getValue('size')).($admin->getValue('multiselect')== NULL ? '' : ','.$admin->getValue('multiselect'))
		);
		break;		
		
 	case 'email_recip':	
		$fields = array(
			'value' => $value, 	// *
			'extra'	=> int_not0($admin->getValue('size')).','.$admin->getValue('multiselect')
		);
		break;
		
	case 'checkbox':
	    $sTempSeperator = $admin->getValue("seperator", "string", "post", "X", "");
		if ($sTempSeperator != '')
		{
			$extra = str_replace(array("[[", "]]"), '', $sTempSeperator);
		}
		else
		{
				$extra = "";
		}
		if ($extra == "" and $isnewfield) $extra = "<br />";   // set default value
		
		$fields = array(
			'value' => $value, 	// *
			'extra'	=> $extra
		);
		break;
		
	case 'date':
		$fields = array(
			'value' => str_replace(array("[[", "]]"), '', htmlspecialchars($admin->getValue('value') ?? "", ENT_QUOTES)),
			'extra'	=> int_not0($admin->getValue('length'))
		);
		break;
		
	case 'radio':
		$extra = str_replace(array("[[", "]]"), '', $admin->getValue('seperator') ?? "");
		if ($extra=="" and $isnewfield) $extra = "<br />";   // set default value
		
		$fields = array(
			'value'	=> $value, // *!
			'extra'	=> $extra
		);
		break;
		
	case 'fieldset_start':		
		$fields = array(
			'value'	=> $value, 
			'extra'	=> "" 
		);
		break;

	case 'fieldset_end':
		$fields = array(
			'value'	=> $value, 
			'extra'	=> ""
		);
		break;

		
	default:
		$admin->print_error( "[1] No field-type match!" );
		return 0;
}

$database->build_and_execute(
    "update",
    TABLE_PREFIX."mod_mpform_fields",
    $fields,
    "`field_id`= '".$field_id."'"
);  

// If there is no db error say successful
if (isset($_POST['copy']))
{
	$admin->print_success($TEXT['SUCCESS'].' -> Copy', LEPTON_URL.'/modules/mpform/copy_field.php?page_id='.$page_id.'&section_id='.$section_id.'&oldfield_id='.$fid.'&leptoken='.get_leptoken());
}
elseif (isset($_POST['add']))
{
	$admin->print_success($TEXT['SUCCESS'].' -> Add', LEPTON_URL.'/modules/mpform/add_field.php?page_id='.$page_id.'&section_id='.$section_id.'&leptoken='.get_leptoken());
}
else
{
	$admin->print_success($TEXT['SUCCESS'], LEPTON_URL.'/modules/mpform/modify_field.php?page_id='.$page_id.'&section_id='.$section_id.'&field_id='.$fid.'&leptoken='.get_leptoken());
}

// Print admin footer
$admin->print_footer();
