<?php

/**
 *
 * 
 *  @module         MPForm
 *  @author         Frank Heyne, Dietrich Roland Pehlke, erpe
 *  @license        see info.php of this addon
 *  @platform       see info.php of this addon
 *  @license terms  see info.php of this addon
 *  @version        see info.php of this module
 *  
 *
 */
 
// include secure.php to protect this file and the whole CMS!
if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}
// end include secure.php
 
$module_directory	= 'mpform';
$module_name		= 'MPForm';
$module_function	= 'page';
$module_version		= '2.6.0';
$module_platform	= '7.2';
$module_author		= 'Frank Heyne, erpe, Dietrich Roland Pehlke (last)';
$module_license		= '<a href="https://www.gnu.org/licenses/#GPL">GNU General Public License</a>';
$module_guid		= '3FFE634D-D6BD-4C42-B449-D7A1DE3BA74A';
$module_description = 'This module allows you to create customised online forms. <a href="https://doc.lepton-cms.org/documentation/mp_form/EN/mpform.html" target="_blank">See online docs</a>';
$module_home		= '<a href="http://www.lepton-cms.com">LEPAdoR</a>';

/**
 * Documentation:  https://doc.lepton-cms.org/documentation/mp_form/EN/mpform.html
 */
