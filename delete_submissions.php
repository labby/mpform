<?php

/**
 *
 * 
 *  @module         MPForm
 *  @author         Frank Heyne, Dietrich Roland Pehlke, erpe
 *  @license        see info.php of this addon
 *  @platform       see info.php of this addon
 *  @license terms  see info.php of this addon
 *  @version        see info.php of this module
 *  
 *
 */

if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}

// Include admin wrapper script
require(LEPTON_PATH.'/modules/admin.php');
$oMPFORM = mpform::getInstance();

if(!isset($_POST['marked_submission'])) {
	$admin->print_error($oMPFORM->language['backend']['submission_selected'], ADMIN_URL.'/pages/modify.php?page_id='.$page_id);	
}
if(!isset($_POST['marked_submission']) OR !is_array($_POST['marked_submission'])) {
	header("Location: ".ADMIN_URL."/pages/index.php");
	exit(0);
} else {
	$submission_ids = $_POST['marked_submission'];
}

// save original table before deleting entries
LEPTON_handle::create_sik_table('mod_mpform_submissions');

// delete entries from original table
$database->simple_query("DELETE FROM `".TABLE_PREFIX."mod_mpform_submissions` WHERE `submission_id` IN(".implode(',',$submission_ids).") ");

// Check if there is a db error, otherwise say successful
if($database->is_error()) {
	$admin->print_error($database->get_error(), ADMIN_URL.'/pages/modify.php?page_id='.$page_id);
} else {
	$admin->print_success($TEXT['SUCCESS'].'<br /><br />'.$oMPFORM->language['backend']['delete_submissions'], ADMIN_URL.'/pages/modify.php?page_id='.$page_id);
}

// Print admin footer
$admin->print_footer();

?>