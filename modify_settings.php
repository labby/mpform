<?php

/**
 *  @module         MPForm
 *  @author         Frank Heyne, Dietrich Roland Pehlke, erpe
 *  @license        see info.php of this addon
 *  @platform       see info.php of this addon
 *  @license terms  see info.php of this addon
 *  @version        see info.php of this module
 *
 */

if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}

// get class and admin
$oMPFORM = mpform::getInstance();
$mod_dir = $oMPFORM->module_directory;

//	Get the "settings" from the database table
$settings = array();
$database->execute_query(
	"SELECT * FROM `". TABLE_PREFIX ."mod_mpform_settings` WHERE `section_id` = ".$section_id,
	true,
	$settings,
	false
);

// Protect from cross page reading
if ($settings['page_id'] != $page_id) {  
	header("Location: ".ADMIN_URL."/pages/index.php");
	exit(0);
}

// Get all pages for the "pages" select for the "following" (success) pages 
LEPTON_handle::register( "page_tree" );
$all_pages = array();
page_tree( 0, $all_pages, array('page_id','page_title','menu_title') );

// die( LEPTON_tools::display( $all_pages ));

$email_from_value = $settings['email_from'];
$email_fromname_value = $settings['email_fromname']; 

/**
 *	Any e-mail fields in the current form?
 *
 */
$all_email_fields = array();
$database->execute_query(
	"SELECT `field_id`,`title` FROM `".TABLE_PREFIX."mod_mpform_fields` WHERE `section_id` = ".$section_id." AND (`type` = 'email') ORDER BY `position` ASC",
	true,
	$all_email_fields,
	true
);

foreach($all_email_fields as &$ref) {
	$ref['field_id']= 'field'.$ref['field_id'];
}

/**
 *	Same for the textfields for "names"
 */
$all_text_fields = array();
$database->execute_query(
	"SELECT `field_id`,`title` FROM `".TABLE_PREFIX."mod_mpform_fields` WHERE `section_id` = ".$section_id." AND (`type` = 'textfield') ORDER BY `position` ASC",
	true,
	$all_text_fields,
	true
);

foreach($all_text_fields as &$ref) {
	$ref['field_id']= 'field'.$ref['field_id'];
}
	 
// Collecting the values for the template
$form_values = array(
	'settings'			=> $settings,
	'oMPF'				=> $oMPFORM,	
	'all_pages'			=> $all_pages,
	'all_email_fields'	=> $all_email_fields,
	'all_text_fields'	=> $all_text_fields,
	'PAGE_ID'			=> (int) $page_id,
	'SECTION_ID'		=> (int) $section_id,
	'MOD_SAVE_URL'		=> LEPTON_URL. str_replace("\\","/",substr(dirname(__FILE__),strlen(LEPTON_PATH))).'/save_settings.php'
);


//	Get the templatge-engine
$oTWIG = lib_twig_box::getInstance();
$oTWIG->registerModule( $mod_dir );

echo $oTWIG->render(
	'@mpform/backend_modify_settings.lte',
	$form_values
);
// Print admin footer
$oMPFORM->admin->print_footer();
?>