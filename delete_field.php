<?php

/**
 *
 * 
 *  @module         MPForm
 *  @author         Frank Heyne, Dietrich Roland Pehlke, erpe
 *  @license        see info.php of this addon
 *  @platform       see info.php of this addon
 *  @license terms  see info.php of this addon
 *  @version        see info.php of this module
 *  
 *
 */

if(!defined("SEC_FILE")){define("SEC_FILE",'/framework/secure.php' );}
if (defined('LEPTON_PATH')) {  
  include LEPTON_PATH.SEC_FILE;
} else {
  $oneback = "../";
  $root = $oneback;
  $level = 1;
  while (($level < 10) && (!file_exists($root.SEC_FILE))) {
    $root .= $oneback;
    $level += 1;
  }
  if (file_exists($root.SEC_FILE)) { 
    include $root.SEC_FILE;   
  } else {
    trigger_error(sprintf("[ <b>%s</b> ] Can't include secure.php!", $_SERVER['SCRIPT_NAME']), E_USER_ERROR);
  }
}



// Include admin wrapper script
$update_when_modified = true; // Tells script to update when this page was last updated
require LEPTON_PATH.'/modules/admin.php';

// die(LEPTON_tools::display($_GET,'pre','ui message'));
if (!isset($_GET['field_id']) OR !is_numeric($_GET['field_id'])) {
	header("Location: ".ADMIN_URL."/pages/index.php");
	exit(0);
} else {
	$field_id = intval($_GET['field_id']);
}

// Delete row
$database->simple_query("DELETE FROM `".TABLE_PREFIX."mod_mpform_fields` WHERE `field_id` = ".$field_id." and `section_id` = ".$section_id." ");
$database->simple_query("ALTER TABLE `".TABLE_PREFIX."mod_mpform_results_".$section_id."` drop `field".$field_id."` ");

// we need to get a new leptoken here.
$oSECURE = new LEPTON_securecms();
$newLeptoken = $oSECURE->createLepToken();

// Check if there is a db error, otherwise say successful
if($database->is_error()) {
	$admin->print_error($database->get_error(), ADMIN_URL.'/pages/modify.php?page_id='.$page_id."&leptoken=".$newLeptoken." ");
} else {
	$admin->print_success($TEXT['SUCCESS'], ADMIN_URL.'/pages/modify.php?page_id='.$page_id."&leptoken=".$newLeptoken." ");
}

// Print admin footer
// $admin->print_footer();
